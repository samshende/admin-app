import gql from 'graphql-tag';
export default gql`

mutation updateDoctor(
    $id: ID!
    $mob: Float!
    $email: String!
    $fName: String!
    $lName: String!
    $fullName: String!
    $gender: Gender!
 
  ){
    updateDoctor(input:{
      id: $id
      mob: $mob
      email: $email
      fName: $fName
      lName: $lName
      fullName: $fullName
      gender: $gender
  
    }){
      id
      mob
      email
      fName
      lName
      fullName
      gender
      speciality
      subSpeciality
      clinicName
      addressOne
      addressTwo
      country
      state
      city
      zip
      registrationId
      registrationCouncil
      registrationYear
      highestQualification
      university
      degreeYear
      experienceYears
      identityProofDoc{
        bucket
        key
        region
      }
      registrationProofDoc{
        bucket
        key
        region
      }
      clinicAddressProofDoc{
        bucket
        key
        region
      }
    }
  }
  `
  