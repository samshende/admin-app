<Form onSubmit={this.handleSubmit}>
<Row gutter={16}>
  
    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }} >
        <FormItem label="First name">
            {getFieldDecorator(`fname`, {
                initialValue:data && data.fName?data.fName:'',
                rules: [{
                    required: true,
                    message: "Please enter First name",
                },
                ]
            })(
                <Input placeholder="First name" style={{ width: '100%' }} />
            )}
        </FormItem>
    </Col>

    
    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }} >
        <FormItem label="Last name">
            {getFieldDecorator(`lname`, {
                initialValue:data && data.lName?data.lName:'',

                rules: [{
                    required: true,
                    message: "Please enter Last name",
                },
                ]
            })(
                <Input placeholder="Last name" style={{ width: '100%' }} />
            )}
        </FormItem>
    </Col>
    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }} >
        <FormItem label="Gender">
            {getFieldDecorator(`gender`, {
                initialValue:data && data.gender?data.gender:'',

                rules: [{
                    required: true,
                    message: "Please enter Gender",
                },
                ]
            })(
                <Input placeholder="Gender" style={{ width: '100%' }} />
            )}
        </FormItem>
    </Col>
  
</Row>

<Row gutter={16}>
  
    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }} >
        <FormItem
            label="Email"
        >
            {getFieldDecorator(`email`, {
                initialValue:data && data.email?data.email:'',

                rules: [{
                    type: 'email',
                    required: true,
                    message: "Please enter E-mail!",
                },

                ]
            })(
                <Input placeholder="Email" style={{ width: '100%' }} />
            )}
        </FormItem>
    </Col>
  
        <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }} >
            <FormItem
                label="Mobile Number"
            >
                {getFieldDecorator(`mob`, {
                initialValue:data && data.mob?data.mob:'',

                    rules: [{
                        required: true,
                        message: "Please enter Mobile Number",
                    },
                    {
                        validator: (rule, value, cb) => {
                            if (value != undefined && value != "" && (isNaN(value.trim()) || value.trim().length != 10)) {
                                cb('Please enter 10 digit number only')
                            }
                            cb()
                        }
                    }
                    ]
                })(
                    <Input placeholder="Mobile Number" style={{ width: '100%' }} />
                )}
            </FormItem>
        </Col>
 
        <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }} >
        <FormItem label="Aadhaar number">
            {getFieldDecorator(`Aadhaarnumber`, {
                rules: [{
                    required: true,
                    message: "Please enter Aadhaar number",
                },
                ]
            })(
                <Input placeholder="Aadhaar number" style={{ width: '100%' }} />
            )}
        </FormItem>
    </Col>
    </Row>

    <Row gutter={16}>
    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }} >
        <FormItem label="Aadhaar proof">
            {getFieldDecorator(`AadhaarProof`, {
                initialValue:data && data.identityProofDoc?data.identityProofDoc:'',
                rules: [{
                    required: true,
                    message: "Please upload Aadhaar proof",
                },
                ]
            })
            (
                <Upload
                listType="picture-card"
                defaultFileList={Object.keys(this.state.identityProofDoc).length != 0 ? [this.state.identityProofDoc] : []}
                data={this.state.identityProofDoc}
                onChange={this.handleMainImageideProof}
                onRemove={this.handleMainImageRemoveideProof}
            >
                {Object.keys(this.state.identityProofDoc).length != 0 ? null : uploadButton}
            </Upload>
            )}
        </FormItem>
    </Col>
</Row>

<Row gutter={16}>
  
  <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }} >
      <FormItem label="Highest qualification">
          {getFieldDecorator(`higQual`, {
                initialValue:data && data.highestQualification?data.highestQualification:'',

              rules: [{
                  required: true,
                  message: "Please enter Highest qualification",
              },
              ]
          })(
              <Input placeholder="Highest qualification" style={{ width: '100%' }} />
          )}
      </FormItem>
  </Col>
  <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }} >
      <FormItem label="University">
          {getFieldDecorator(`University`, {
                initialValue:data && data.university?data.university:'',

              rules: [{
                  required: true,
                  message: "Please enter University",
              },
              ]
          })(
              <Input placeholder="University" style={{ width: '100%' }} />
          )}
      </FormItem>
  </Col>
  <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }} >
      <FormItem label="Degree year">
          {getFieldDecorator(`Degreeyear`, {
                initialValue:data && data.degreeYear?data.degreeYear:'',

              rules: [{
                  required: true,
                  message: "Please enter Degree year",
              },
              ]
          })(
              <Input placeholder="Degree year" style={{ width: '100%' }} />
          )}
      </FormItem>
  </Col>

</Row>

<Row gutter={16}>
  
    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }} >
        <FormItem label="Speciality">
            {getFieldDecorator(`Speciality`, {
                initialValue:data && data.speciality?data.speciality:'',

                rules: [{
                    required: true,
                    message: "Please enter Speciality",
                },
                ]
            })(
                <Input placeholder="Speciality" style={{ width: '100%' }} />
            )}
        </FormItem>
    </Col>
    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }} >
        <FormItem label="Registration number">
            {getFieldDecorator(`regNo`, {
                initialValue:data && data.registrationId?data.registrationId:'',

                rules: [{
                    required: true,
                    message: "Please enter Registration number",
                },
                ]
            })(
                <Input placeholder="Registration number" style={{ width: '100%' }} />
            )}
        </FormItem>
    </Col>
    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }} >
        <FormItem label="Registration council">
            {getFieldDecorator(`RegCouncil`, {
                initialValue:data && data.registrationCouncil?data.registrationCouncil:'',

                rules: [{
                    required: true,
                    message: "Please enter Registration council",
                },
                ]
            })(
                <Input placeholder="Registration council" style={{ width: '100%' }} />
            )}
        </FormItem>
    </Col>
  
</Row>

<Row gutter={16}>
  
    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }} >
        <FormItem label="Registration year">
            {getFieldDecorator(`regYear`, {
                initialValue:data && data.registrationYear?data.registrationYear:'',

                rules: [{
                    required: true,
                    message: "Please enter Registration year",
                },
                ]
            })(
                <Input placeholder="Registration year" style={{ width: '100%' }} />
            )}
        </FormItem>
    </Col>
    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }} >
        <FormItem label="Yearsofexperience">
            {getFieldDecorator(`experienceYears`, {
                initialValue:data && data.experienceYears?data.experienceYears:'',

                rules: [{
                    required: true,
                    message: "Please enter Years of experience",
                },
                ]
            })(
                <Input placeholder="Years of experience" style={{ width: '100%' }} />
            )}
        </FormItem>
    </Col>
    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }} >
        <FormItem label="Registration proof">
            {getFieldDecorator(`Registrationproof`, {
                initialValue:data && data.registrationProofDoc?data.registrationProofDoc:'',

                rules: [{
                    required: true,
                    message: "Please Upload Registration proof",
                },
                ]
            })(
                <Upload
                listType="picture-card"
                defaultFileList={Object.keys(this.state.registrationProofDoc).length != 0 ? [this.state.registrationProofDoc] : []}
                data={this.state.registrationProofDoc}
                onChange={this.handleMainImageregProof}
                onRemove={this.handleMainImageRemoveregProof}
            >
                {Object.keys(this.state.registrationProofDoc).length != 0 ? null : uploadButton1}
            </Upload>

            )}
        </FormItem>
    </Col>
  
</Row>
<Row gutter={16}>
  
  <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }} >
      <FormItem label="Clinic name">
          {getFieldDecorator(`cliName`, {
                initialValue:data && data.clinicName?data.clinicName:'',

              rules: [{
                  required: true,
                  message: "Please enter Clinic name",
              },
              ]
          })(
              <Input placeholder="Clinic name" style={{ width: '100%' }} />
          )}
      </FormItem>
  </Col>
  <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }} >
      <FormItem label="Pin code">
          {getFieldDecorator(`zip`, {
                initialValue:data && data.zip?data.zip:'',

              rules: [{
                  required: true,
                  message: "Please enter Pin code",
              },
              ]
          })(
              <Input placeholder="Pin code" style={{ width: '100%' }} />
          )}
      </FormItem>
  </Col>
  <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }} >
      <FormItem label="Building">
          {getFieldDecorator(`Building`, {
                initialValue:data && data.addressTwo?data.addressTwo:'',

              rules: [{
                  required: true,
                  message: "Please enter Building",
              },
              ]
          })(
              <Input placeholder="Building" style={{ width: '100%' }} />
          )}
      </FormItem>
  </Col>

</Row>
<Row gutter={16}>
  
    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }} >
        <FormItem label="Street">
            {getFieldDecorator(`Street`, {
                initialValue:data && data.zip?data.zip:'',

                rules: [{
                    required: true,
                    message: "Please enter Street",
                },
                ]
            })(
                <Input placeholder="Street" style={{ width: '100%' }} />
            )}
        </FormItem>
    </Col>
    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }} >
        <FormItem label="City">
            {getFieldDecorator(`City`, {
                initialValue:data && data.city?data.city:'',

                rules: [{
                    required: true,
                    message: "Please enter City",
                },
                ]
            })(
                <Input placeholder="City" style={{ width: '100%' }} />
            )}
        </FormItem>
    </Col>
    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }} >
        <FormItem label="State">
            {getFieldDecorator(`State`, {
                initialValue:data && data.state?data.state:'',

                rules: [{
                    required: true,
                    message: "Please enter State",
                },
                ]
            })(
                <Input placeholder="State" style={{ width: '100%' }} />
            )}
        </FormItem>
    </Col>
  
</Row>
<Row gutter={16}>
    <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }} xl={{ span: 8 }} >
        <FormItem label="Clinic address proof">
            {getFieldDecorator(`clinicAdd`, {
                initialValue:data && data.clinicAddressProofDoc?data.clinicAddressProofDoc:'',
                rules: [{
                    required: true,
                    message: "Please upload Clinic address proof",
                },
                ]
            })(
                <Upload
                listType="picture-card"
                defaultFileList={Object.keys(this.state.clinicAddressProofDoc).length != 0 ? [this.state.clinicAddressProofDoc] : []}
                data={this.state.clinicAddressProofDoc}
                onChange={this.handleMainImageAddProof}
                onRemove={this.handleMainImageRemoveAddProof}
            >
                {Object.keys(this.state.clinicAddressProofDoc).length != 0 ? null : uploadButton2}
            </Upload>
            )}
        </FormItem>
    </Col>
</Row>




    <Row gutter={16}>
        <Col span={24}>
            <Button loading={this.state.loading} type="primary" htmlType="submit" style={{ background: "#389e0d", color: "#fff", marginBottom: "5%", marginRight: "20px" }} >
                Submit
            </Button>
            <Link to={{ pathname: '/' }}>
                <Button type="default" style={{ background: "#f44336", color: "#fff", marginBottom: "5%" }}   >Cancel</Button>
            </Link>
        </Col>
    </Row>
    {/* </Card> */}

</Form>
